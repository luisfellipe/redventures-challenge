import React, { useState, useEffect } from "react";
import styled from "styled-components";
import logo from "../assets/logo/logo-greenthumb.svg";
import homeplant from "../assets/illustrations/illustration-home.png";
import Sol from "./Sol";
import Regamento from "./Regamento";
import Animais from "./Animais";
import ListaDePlantas from "./ListaDePlantas"
import PaginaDeCompra from './PaginaDeCompra'

const Tudo = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  position: relative;
  align-items:center;
  background-color:#F6F6F6;
  @media (max-width:425px) {
    text-align:-webkit-center;
    align-items:center;
    justify-content:center;
  }
`;

const Body = styled.div`
  display:flex;
  flex-direction: row;
  align-items:center;
  justify-content:center;
  margin: 15px;
  @media (max-width:425px) {
    flex-direction:column;
    align-items:center;
  }
`;

const Title = styled.img`
  position: relative;
  height: 25px;
  margin:15px;
`;

const TextBody = styled.div`
  font-family: Helvetica;
  font-size: 66px;
  color: #0c261c;
  position: relative;
  display: flex;
  flex-direction: row;
  position:relative;
  @media (max-width:768px){
  font-size: 35px;
  }
  @media (max-width:425px) {
    margin-bottom:121px;
  }
`;

const Img = styled.img`
  position: relative;
  width: 779px;
  height: 738px;
  right: 8em;
  @media (max-width:1024px) {
    right:0px;
    width:575px;
  }
  @media (max-width:768px) {
    height:378px;
    width:334px;
  }
`;

const Header = styled.div`
  display: flex;
  margin: 5px;
  padding: 10px;
  @media (max-width:425px) {
    justify-content:center;
  }
`;

const Button = styled.div`
  border-radius: 36px;
  background: #15573f;
  border: 0 solid #ffffff;
  font-family: Helvetica;
  font-size: 16px;
  margin: 15px;
  color: #ffffff;
  text-align: center;
  line-height: 3em;
  width: 170px;
  height: 50px;
  position: relative;
  bottom: 15em;
  @media (max-width:768px) {
    bottom:8em;
  }
  @media (max-width:425px) {
font-size: 16px;
bottom:30em;
justify-content:center;
align-items:center;
  }
`;

const Home = () => {
  const [quiz, setquiz] = useState({
    qualpergunta: 0,
    pergunta1: '',
    pergunta2: '',
    pergunta3: undefined
  });
  const [plantas,setplantas] = useState([])
  const pegarResultados = (sol,regamento,animais) =>{
    fetch(`https://6nrr6n9l50.execute-api.us-east-1.amazonaws.com/default/front-plantTest-service?sun=${sol}&water=${regamento}&pets=${animais}`)
    .then(res => res.json())
    .then(
      (result) => {
        console.log('blabla',result)
        setplantas(result)
      },
      (error) => {
        console.log('erro',error)
      }
    )
  }
  useEffect(()=>{
    console.log(quiz)
      if(quiz.pergunta1 !== '' 
        && quiz.pergunta2 !== ''
        && quiz.pergunta3 !== undefined
        && quiz.qualpergunta === 4)
      pegarResultados(quiz.pergunta1, quiz.pergunta2, quiz.pergunta3)
},[quiz]       
  )
  return (
    <div>
      <Tudo>
        {quiz.qualpergunta === 0 && (
          <div>
            <Header>
                <Title src={logo} alt="Logo" />
            </Header>
            <Body>
            <TextBody>
              Find your next green friend
              </TextBody>
                <Img src={homeplant} alt="Home plant" />
                </Body>
            <Button
              onClick={() =>
                setquiz({
                  ...quiz,
                  qualpergunta: 1
                })
              }
            >
              start quizz
            </Button>
            
          </div>
        )}
        {quiz.qualpergunta === 1 && (
          <Sol
            resposta={quiz.pergunta1}
            setresposta={resp =>
              setquiz({
                ...quiz,
                pergunta1: resp
              })
            }
            setnext={resp =>
              setquiz({
                ...quiz,
                qualpergunta: 2
              })
            }
            setback={resp =>
              setquiz({
                ...quiz,
                qualpergunta: 0
              })
            }
          />
        )}
        {quiz.qualpergunta === 2 && (
          <Regamento
            resposta={quiz.pergunta2}
            setresposta={resp =>
              setquiz({
                ...quiz,
                pergunta2: resp
              })
            }
            setnext={resp =>
              setquiz({
                ...quiz,
                qualpergunta: 3
              })
            }
            setback={resp =>
              setquiz({
                ...quiz,
                qualpergunta: 1
              })
            }
          />
        )}
        {quiz.qualpergunta === 3 && (
          <Animais
            resposta={quiz.pergunta3}
            setresposta={resp =>
              setquiz({
                ...quiz,
                pergunta3: resp
              })
            }
            setnext={resp =>
              setquiz({
                ...quiz,
                qualpergunta: 4
              })
            }
            setback={resp =>
              setquiz({
                ...quiz,
                qualpergunta: 2
              })
            }
          />
        )}
        {quiz.qualpergunta === 4 && (
          <ListaDePlantas
          setnext={index =>
            setquiz({
              ...quiz,
              qualpergunta: 5,
              qualplanta:index
            })
          }
            plantas={plantas}
          />
        )}
       {quiz.qualpergunta === 5 && (
        <PaginaDeCompra 
          planta={plantas[quiz.qualplanta]}
        />
       )} 
      </Tudo>
    </div>
  );
};

export default Home;
