import React from "react";
import styled from "styled-components";
import sun from "../assets/icons/grey/HighSun.png";
import pet from "../assets/icons/grey/Pet.png";
import drop from "../assets/icons/grey/OneDrop.png";

const Tudo = styled.div`
  background-color: white;
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  align-items: center;
  justify-content: center;
  width: 377px;
  margin-right: 250px;
  margin-left:164px;
  @media (max-width: 425px) {
    margin:0px;
  }
`;

const Titulo = styled.div`
  font-family: Helvetica;
  font-size: 50px;
  color: #0c261c;
  line-height: 55px;
`;

const Preco = styled.div`
  font-family: Helvetica;
  font-size: 24px;
  color: #d6d6d6;
  text-align:left;
`;

const Detalhes = styled.div`
  font-family: Helvetica;
  font-size: 16px;
  color: #6e6e6e;
  height: 105px;
`;

const DetalhesImg = styled.img`
  margin-right: 14px;
  height: 25px;
  margin-bottom:17px;
  @media (max-width: 425px) {
    margin-left:-15px;
  }
`;

const DetalhesImgSol = styled.img`
  margin-right: 14px;
  height: 25px;
  margin-bottom:17px;
  @media (max-width: 425px) {
    margin-left:-9px;
  }
`;

const DetalhesImgPet = styled.img`
  margin-right: 8px;
  margin-left:-2px;
  height: 25px;
  margin-bottom:17px;
  @media (max-width: 425px) {
    margin-left:20px;
  }
`;

const CartaoPlanta = styled.div``;

const PlantaImagem = styled.img`
  height: 200px;
`;

const Planta = props => {
    const sunmap = (sunstat) =>{
        if (sunstat === "high") return "High sunlight"
        if (sunstat === "low") return "Low sunlight"
        if (sunstat === "no") return "No sunlinght"
    }
    const watermap = (waterstat) => {
        if (waterstat === "rarely") return "Water rarely"
        if (waterstat === "regularly") return "Water regularly"
        if (waterstat === "daily") return "Water daily"
    }
    const petmap = (petstat) => {
        if (petstat === false) return "Non-toxic for pets"
        if (petstat === true) return "Toxic for pets"
    }
  return (
    <Tudo>
      <Titulo>{props.planta.name}</Titulo>
      <Preco>${props.planta.price}</Preco>
      <CartaoPlanta>
        <PlantaImagem src={props.planta.url} alt="planta imagem" />
        <Detalhes>
          <DetalhesImgSol src={sun} alt="solzinho" />
          {sunmap(props.planta.sun)}
          <br />
          <DetalhesImg src={drop} alt="gota" />
          {watermap(props.planta.water)}
          <br />
          <DetalhesImgPet src={pet} alt="pet" />
          {petmap(props.planta.toxicity)}
        </Detalhes>
      </CartaoPlanta>
    </Tudo>
  );
};

export default Planta;
