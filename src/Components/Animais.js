import React,{useState} from "react";
import logo from '../assets/logo/logovertical.png'
import logomobile from '../assets/logo/logo.png'
import animalogo from "../assets/illustrations/dog.png";
import pet from "../assets/icons/coral/Pet.png";
import wpet from "../assets/icons/white/Pet.png";
import semresp from "../assets/icons/coral/NoSun.png";
import wsemresp from "../assets/icons/white/NoSun.png";
import styled from "styled-components";

const Tudo = styled.div`
  display: flex;
  flex-direction: row;
  width:100%;
  background-color: #f6f6f6;
  @media (max-width: 425px) {
    flex-direction: column;
  }
`;

const Conteudo = styled.div`
display: flex;
flex-direction: column;
width:100%;
justify-content: center;
padding-top: 3em;
background-color: #f6f6f6;
@media (max-width: 425px) {
  flex-direction: column;
}
`;

const ImgLateral = styled.div`
display:flex;
flex-direction:column;
margin-left:81px;
margin-top:61px;
@media (max-width: 425px) {
 margin-left:0px;
 margin-top:0px;
}
`;

const LogoLateral = styled.img`
width:25px;
@media (max-width: 425px) {
  display:none;
}
`;

const LogoMobile = styled.img`
  display:none;
  @media (max-width: 425px) {
    display:block;
    width:166px;
    margin-top:24px;
  }
`;

const LinhaVertical = styled.div`
border-left: 1px solid #15573F;
height: 70%;
width:26px;
margin-top: 39px;
margin-left:11px;
@media (max-width: 425px) {
display:none;
}
`;

const Cabecalho = styled.div`
  justify-content: center;
  display: flex;
  flex-direction: column;
  position: relative;
  margin-bottom: 65px;
  @media (max-width: 425px) {
    margin-bottom: 31px;
  }
`;

const Animal = styled.img`
  align-self: center;
  height: 86px;
  margin-bottom: 30px;
  @media (max-width: 425px) {
    height: 74px;
  }
`;

const CorpoBotao = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: center;
  @media (max-width: 425px) {
    flex-direction: column;
    align-items: center;
  }
`;

const BotaoSim = styled.div`
  height: 215px;
  width: 190px;
  color: ${props => (props.selecionado ? '#ffffff' : '#6e6e6e')};
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  box-shadow: ${props => (props.selecionado ? '0px 22px 24px 0px #F6D7CB': '0px 22px 24px 0px lightgrey')};
  border-radius: 3px;
  display: flex;
  flex-direction: column;
  text-align: center;
  justify-content:center;
  line-height: 70px;
  align-items: center;
  background-color: ${props => (props.selecionado ? "#FD9872" : "#ffffff")};
  @media (max-width:425px) {
    flex-direction: row;
    width:80%;
    height: 74px;
  }
`;

const BotaoNao = styled.div`
  height: 215px;
  width: 190px;
  color: ${props => (props.selecionado ? '#ffffff' : '#6e6e6e')};
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  box-shadow: ${props => (props.selecionado ? '0px 22px 24px 0px #F6D7CB': '0px 22px 24px 0px lightgrey')};
  border-radius: 3px;
  display: flex;
  flex-direction: column;
  text-align: center;
  justify-content:center;
  line-height: 70px;
  align-items: center;
  background-color: ${props => (props.selecionado ? "#FD9872" : "#ffffff")};
  @media (max-width:425px) {
    flex-direction: row;
    width:80%;
    height: 74px;
  }
`;
const ImagemBotaoSim = styled.img`
  height: 68px;
  @media (max-width: 425px) {
    height: 34px;
    margin-right: 20px;
    width:45px;
  }
`;

const ImagemBotao = styled.img`
  height: 68px;
  @media (max-width: 425px) {
    height: 42px;
    margin-right: 20px;
    width:42px;
  }
`;

const BotaoAnterior = styled.div`
  border-radius: 36px;
  background: #ffffff;
  border: 1px solid #15573f;
  font-family: Helvetica;
  font-size: 16px;
  color: #15573f;
  text-align: center;
  width: 170px;
  height: 50px;
  line-height: 3em;
  position: relative;
  right: 13em;
  background-color: #f6f6f6;
  &:hover {
    color: #ffffff;
    background-color: #15573f;
  }
  @media (max-width: 425px) {
    flex-direction: row;
    width: 146px;
    height: 50px;
    right: 0px;
    &:hover {
      color: #ffffff;
      background-color: #15573f;
    }
  }
`;

const Texto = styled.p`
margin:0;
`;

const BotaoProximo = styled.div`
  border-radius: 36px;
  background: #ffffff;
  border: 1px solid #15573f;
  font-family: Helvetica;
  font-size: 16px;
  color: #15573f;
  text-align: center;
  width: 170px;
  height: 50px;
  position: relative;
  line-height: 3em;
  left: 13em;
  background-color: #f6f6f6;
  &:hover {
    color: #ffffff;
    background-color: #15573f;
  }
  @media (max-width: 425px) {
    flex-direction: row;
    margin-bottom:31px;
    width: 146px;
    height: 50px;
    left: 0px;
    &:hover {
      color: #ffffff;
      background-color: #15573f;
    }
  }
`;

const CorpoBotao2 = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  margin-top: 46px;
  margin-bottom:31px;
  justify-content: center;
  @media (max-width: 425px) {
    flex-direction: column;
    align-items: center;
  }
`;

const Titulo = styled.div`
  font-family: Helvetica;
  font-size: 35px;
  color: #6e6e6e;
  text-align: center;
  line-height: 54px;
`;

const Subtitulo = styled.div`
  font-family: Helvetica;
  font-size: 14px;
  color: #6e6e6e;
  text-align: center;
  line-height: 39px;
`;

const ContainerTexto = styled.div`
display:flex;
flex:1;
`;

const Animais = props => {
  const [selecionado, setselecionado] = useState(0);
  const statusselecionado = (index, resposta) =>
    !!(selecionado === index || props.resposta === resposta);
  return (
    <Tudo>
      <ImgLateral>
        <LogoLateral src={logo} alt="logo"/>
        <LogoMobile src={logomobile} alt="logo2" />
        <LinhaVertical/>
      </ImgLateral>
      <Conteudo>
      <Cabecalho>
          <Animal src={animalogo} alt="logo animais" />
          <Titulo>Do you have pets? Do they chew plants?</Titulo>
          <Subtitulo>
            We are asking because some plants can be toxic for your buddy.
          </Subtitulo>
      </Cabecalho>
      <CorpoBotao>
        <br></br>
        
        <BotaoSim
          onClick={() => props.setresposta(true)}
          onMouseEnter={() => setselecionado(1)}
          onMouseLeave={() => setselecionado(0)}
          selecionado={statusselecionado(1, true)}
        >
          {!statusselecionado(1, true) && (
            <ImagemBotaoSim src={pet} alt="dog" />
          )}
          {statusselecionado(1, true) && (
            
            <ImagemBotaoSim src={wpet} alt="whitedog" />
          )}
          <ContainerTexto><Texto>Yes</Texto></ContainerTexto>
          
        </BotaoSim>
        
        <BotaoNao
          onClick={() => props.setresposta(false)}
          onMouseEnter={() => setselecionado(2)}
          onMouseLeave={() => setselecionado(0)}
          selecionado={statusselecionado(2, false)}
        >
          {!statusselecionado(2, false) && (
            <ImagemBotao src={semresp} alt="no" />
          )}
          {statusselecionado(2, false) && (
            <ImagemBotao src={wsemresp} alt="no" />
          )}
         <ContainerTexto><Texto>No/They don't care</Texto></ContainerTexto>
        </BotaoNao>
      </CorpoBotao>
      <CorpoBotao2>
      <BotaoProximo onClick={props.setnext}>finish</BotaoProximo>
        <BotaoAnterior onClick={props.setback}>previous</BotaoAnterior>
      </CorpoBotao2>
      </Conteudo>
    </Tudo>
  );
};

export default Animais;
