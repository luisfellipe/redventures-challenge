import React from "react";
import styled from "styled-components";
import hsun from '../assets/icons/grey/HighSun.png'
import lsun from '../assets/icons/grey/LowSun.png'
import nsun from '../assets/icons/grey/NoSun.png'
import drop from '../assets/icons/grey/OneDrop.png'
import tdrop from '../assets/icons/grey/TwoDrops.png'
import thdrop from '../assets/icons/grey/ThreeDrops.png'
import toxic from '../assets/icons/grey/toxic.png'




const Body = styled.div`
display flex;
flex-direction: column;
justify-content:center;
align-items:center;
`;

const Card = styled.div`
width:268px;
height:390px;
display:flex;
flex-direction:column;
text-align:center;
justify-content:center;
background: #FFFFFF;
box-shadow: 0 28px 38px 0 rgba(0,0,0,0.09);
margin-right:30px;
margin-bottom:30px;
@media (max-width: 425px) {
  margin-right:0px;
  align-items:center;
}
`;

const Preco = styled.div`
font-family: Helvetica;
font-size: 18px;
color: #6E6E6E;
line-height: 28px;
align-self:flex-start;
margin-left: 33px;
margin-right:56px;

`;

const Button = styled.div`
  border-radius: 36px;
  display: flex;
  align-content: center;
  background: #ffffff;
  border: 1px solid #15573f;
  font-family: Helvetica;
  font-size: 16px;
  color: #15573f;
  text-align: center;
  justify-content: center;
  width: 80%;
  height: 50px;
  position: relative;
  line-height: 3em;
  margin-left:26px;
  @media (max-width: 425px) {
    margin-left:0px;
  }
`;

const NomePlanta =  styled.div`
font-family: Helvetica;
font-size: 18px;
color: #15573F;
line-height: 28px;
align-self:flex-start;
margin-left: 32px;
`;

const MapaPlantas = styled.div`
    display:flex;
    flex-direction:row;
`;

const ImgMini = styled.img`
height:23px;
margin-right:9px;
`;

const CartaoDePlantas = props => {
  const sunmap = (sunstat) =>{
    if (sunstat === "high") return <ImgMini src={hsun} alt="high sun" />
    if (sunstat === "low") return <ImgMini src={lsun} alt="low sun" />
    if (sunstat === "no") return <ImgMini src={nsun} alt="no sun" />
}
const watermap = (waterstat) => {
    if (waterstat === "rarely") return <ImgMini src={drop} alt="one drop" />
    if (waterstat === "regularly") return <ImgMini src={tdrop} alt="two drops" />
    if (waterstat === "daily") return <ImgMini src={thdrop} alt="three drops" />
}
const petmap = (petstat) => {
    if (petstat === false) return ""
    if (petstat === true) return <ImgMini src={toxic} alt="toxicp" />
}
  return (
      <Body>
      <Card>
          <img src={props.planta.url}  alt="planta imagem" />
          <NomePlanta>{props.planta.name}</NomePlanta>
          <MapaPlantas>
            <Preco>${props.planta.price}</Preco>
            {petmap(props.planta.toxicity)}
            {sunmap(props.planta.sun)}
            {watermap(props.planta.water)}
            </MapaPlantas>
          <Button onClick={()=> props.setnext(props.index)}>buy now!</Button>
      </Card>
      </Body>
  );
};

export default CartaoDePlantas;
