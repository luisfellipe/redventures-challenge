import React, { useState, useEffect } from "react";
import styled from "styled-components";
import logo from "../assets/logo/logovertical.png";
import logomobile from "../assets/logo/logo.png";
import Planta from "./Planta";
import carta from "../assets/illustrations/envelop.png";

const Tudo = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  background-color: #ffffff;
  @media (max-width: 425px) {
    flex-direction: column;
  }
`;

const Conteudo = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  padding-top: 3em;
  background-color: white;
  @media (max-width: 425px) {
    flex-direction: column;
    padding-top: 0px;
  }
`;

const ImgLateral = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 81px;
  margin-top: 61px;
  @media (max-width: 425px) {
    margin-left: 0px;
    margin-top: 0px;
  }
`;

const LogoLateral = styled.img`
  width: 25px;
  @media (max-width: 425px) {
    display: none;
  }
`;

const LogoMobile = styled.img`
  display: none;
  @media (max-width: 425px) {
    display: block;
    width: 166px;
    margin-top: 24px;
  }
`;

const LinhaVertical = styled.div`
  border-left: 1px solid #15573f;
  height: 70%;
  width: 26px;
  margin-top: 39px;
  margin-left: 11px;
  @media (max-width: 425px) {
    display: none;
  }
`;
const Formulario = styled.div`
  background: #f6f6f6;
  justify-content: center;
  align-items: center;
  width: 381px;
  height: 512px;
  text-align: -webkit-right;
  align-self: center;
  @media (max-width: 425px) {
    width: 100%;
  }
`;
const Titulo = styled.div`
  font-family: Helvetica;
  font-size: 40px;
  color: #0c261c;
  line-height: 37px;
  margin-bottom: 16px;
  margin-top: 43px;
  text-align: center;
`;

const Subtitulo = styled.div`
  font-family: Helvetica;
  font-size: 16px;
  color: #6e6e6e;
  line-height: 29px;
  margin-bottom: 30px;
  text-align: center;
  width: 265px;
  text-align: center;
  @media (max-width: 425px) {
    width: 100%;
    padding:5px;
  }
`;

const InputTitulo = styled.div`
  font-family: Helvetica;
  font-size: 16px;
  color: ${props => (props.erro ? "red" : "#6E6E6E")};
  line-height: 27px;
  text-align: left;
  margin-bottom: 8px;
  margin-left: 62px;
`;

const Input = styled.input`
  line-height: 27px;
  font-family: Helvetica;
  font-size: 14px;
  color: #000000;
  line-height: 27px;
  background: #ffffff;
  width: 95%;
  height: 40px;
  border-radius: 50px;
  border-width: 1px;
  padding:5px;
  outline: none;
  border-color: ${props => (props.erro ? "red" : "white")};
  margin-bottom: 24px;
  ::placeholder {
    padding:5px;
    font-size: 14px;
    color: #D0D0D0;
  }
`;

const Form = styled.div`
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const Envelope = styled.img`
  height: 238px;
`;

const CorpoImg = styled.div`
  text-align: center;
`;

const CorpoTexto = styled.div`
  width: 381px;
  margin: auto;
  text-align: -webkit-center;
`;

const Botao = styled.div`
  border-radius: 36px;
  background: #ffffff;
  border: 1px solid #15573f;
  font-family: Helvetica;
  font-size: 16px;
  color: #ffffff;
  text-align: center;
  margin-right: 45px;
  width: 170px;
  height: 50px;
  line-height: 3em;
  background-color: #15573f;
  @media (max-width: 425px) {
    width: 80%;
  }
`;

const CartaoAgradecimento = styled.div`
  background: #f6f6f6;
  justify-content: center;
  align-items: center;
  width: 381px;
  height: 512px;
  text-align: -webkit-right;
  align-self: center;
  @media (max-width: 425px) {
    width: 100%;
  }
`;

const PaginaDeCompra = props => {
  console.log(" nome planta", props.planta);
  const [enviado, setenviado] = useState(false);
  const [input, setinput] = useState({
    email: "",
    nome: ""
  });
  const [erro, seterro] = useState("");
  useEffect(() => console.log("input", input), [input]);
  const validaemail = email => {
    if (email === undefined) throw new Error("email não pode ser vazio");
    const EmailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!EmailRegex.test(input.email)) throw new Error("email invalido");
  };
  const enviar = () => {
    console.log("enviado");
    try {
      validaemail(input.email);
      setenviado(true);
    } catch (error) {
      console.log("erro", error);
      seterro(error);
    }
  };
  return (
    <Tudo>
      <ImgLateral>
        <LogoLateral src={logo} alt="logo" />
        <LogoMobile src={logomobile} alt="logo2" />
        <LinhaVertical />
      </ImgLateral>
      <Conteudo>
        <Planta planta={props.planta} />
        {!enviado && (
          <Formulario>
            <CorpoTexto>
              <Titulo>Nice pick!</Titulo>
              <Subtitulo>
                Tell us your name and e-mail and we will get in touch regarding
                your order ;)
              </Subtitulo>
            </CorpoTexto>
            <Form>
              <InputTitulo>Name</InputTitulo>
              <Input
                placeholder="Name"
                value={input.nome}
                onChange={e => {
                  setinput({
                    ...input,
                    nome: e.target.value
                  });
                }}
              />
              <InputTitulo
                value={input.email}
                erro={erro !== ""}
                onChange={e => {
                  setinput({
                    ...input,
                    email: e.target.value
                  });
                }}
              >
                E-mail
              </InputTitulo>
              <Input
                placeholder="youremail@youremail.com"
                value={input.email}
                erro={erro !== ""}
                onChange={e => {
                  setinput({
                    ...input,
                    email: e.target.value
                  });
                }}
              />
            </Form>
            <Botao onClick={enviar}>send</Botao>
          </Formulario>
        )}
        {enviado && (
          <CartaoAgradecimento>
            <CorpoTexto>
              <Titulo>Thank you!</Titulo>
              <Subtitulo>
                You will hear from us soon. Please check your e-mail!
              </Subtitulo>
            </CorpoTexto>
            <CorpoImg>
              <Envelope src={carta} alt="envelope" />
            </CorpoImg>
          </CartaoAgradecimento>
        )}
      </Conteudo>
    </Tudo>
  );
};

export default PaginaDeCompra;
