import React from 'react'
import styled from 'styled-components'
import logo from '../assets/logo/logovertical.png'
import logomobile from '../assets/logo/logo.png'
import escolha from '../assets/illustrations/pick.png'
import CartaoDePlantas from './CartaoDePlantas'

const Tudo = styled.div`
display: flex;
flex-direction: row;
width: 100%;
justify-content: center;
background-color: #f6f6f6;
@media (max-width: 425px) {
  flex-direction: column;
}
`;

const Conteudo = styled.div`
display: flex;
flex-direction: column;
width:100%;
justify-content: center;
padding-top: 3em;
background-color: #f6f6f6;
@media (max-width: 425px) {
  flex-direction: column;
}
`;

const ImgLateral = styled.div`
display:flex;
flex-direction:column;
margin-left:81px;
margin-top:61px;
@media (max-width: 425px) {
 margin-left:0px;
 margin-top:0px;
}
`;

const LogoLateral = styled.img`
width:25px;
@media (max-width: 425px) {
  display:none;
}
`;

const LogoMobile = styled.img`
  display:none;
  @media (max-width: 425px) {
    display:block;
    width:166px;
    margin-top:24px;
    margin-bottom:38px;
  }
`;

const LinhaVertical = styled.div`
border-left: 1px solid #15573F;
height: 70%;
width:26px;
margin-top: 39px;
margin-left:11px;
@media (max-width: 425px) {
display:none;
}
`;

const Cabecalho = styled.div`
justify-content: center;
display: flex;
flex-direction: column;
position: relative;
margin-bottom: 65px;
@media (max-width: 425px) {
  margin-bottom: 31px;
}
`;

const Pick = styled.img`
align-self: center;
height: 112px;
margin-bottom: 48px;
@media (max-width: 425px) {
  height: 72px;
  margin-bottom:29px;
}
`;

const Titulo = styled.div`
font-family: Helvetica;
font-size: 65px;
color: #0C261C;
text-align: center;
bottom-margin:79px;
@media (max-width: 425px) {
    font-size: 40px;
    margin-bottom:28px;
}
`;

const Container = styled.div`
display:flex;
flex-direction:row;
flex-wrap:wrap;
margin-right:30px;
justify-content:center;
@media (max-width: 425px) {
    margin-right:0px;
    flex-direction: column;
    margin-bottom:30px;
}
`;

const ListaDePlantas = (props) => {
    return(
        <Tudo>
        <ImgLateral>
        <LogoLateral src={logo} alt="logo"/>
        <LogoMobile src={logomobile} alt="logo2" />
        <LinhaVertical/>
      </ImgLateral>
      <Conteudo>
            <Cabecalho>
                <Pick src={escolha} alt='escolha' />
                <Titulo>
                    Ours pick for you
                </Titulo>
            </Cabecalho>
        <Container>
            {props.plantas.map((elemento,index)=>{
                return(
                    <CartaoDePlantas 
                        setnext={props.setnext}
                        planta={elemento}
                        index={index}
                    />
                )
            })}
        </Container>
        </Conteudo>
        </Tudo>
    )
}

export default ListaDePlantas