import React, { useState } from "react";
import styled from "styled-components";
import logo from '../assets/logo/logovertical.png'
import logomobile from '../assets/logo/logo.png'
import agua from "../assets/illustrations/wateringcan.png";
import gota from "../assets/icons/green/OneDrop.png";
import duasgotas from "../assets/icons/green/TwoDrops.png";
import tresgotas from "../assets/icons/green/ThreeDrops.png";
import gotaw from "../assets/icons/white/OneDrop.png";
import dgotaw from "../assets/icons/white/TwoDrops.png";
import tgotaw from "../assets/icons/white/ThreeDrops.png";

const Tudo = styled.div`
  display: flex;
  flex-direction: row;
  width:100%;
  background-color: #f6f6f6;
  @media (max-width: 425px) {
    flex-direction: column;
  }
`;

const Conteudo = styled.div`
display: flex;
flex-direction: column;
width:100%;
justify-content: center;
padding-top: 3em;
background-color: #f6f6f6;
@media (max-width: 425px) {
  flex-direction: column;
}
`;

const ImgLateral = styled.div`
display:flex;
flex-direction:column;
margin-left:81px;
margin-top:61px;
@media (max-width: 425px) {
 margin-left:0px;
 margin-top:0px;
}
`;

const LogoLateral = styled.img`
width:25px;
@media (max-width: 425px) {
  display:none;
}
`;

const LogoMobile = styled.img`
  display:none;
  @media (max-width: 425px) {
    display:block;
    width:166px;
    margin-top:24px;
  }
`;

const LinhaVertical = styled.div`
border-left: 1px solid #15573F;
height: 70%;
width:26px;
margin-top: 39px;
margin-left:11px;
@media (max-width: 425px) {
display:none;
}
`;

const Cabecalho = styled.div`
  justify-content: center;
  display: flex;
  flex-direction: column;
  position: relative;
  margin-bottom: 65px;
  @media (max-width: 425px) {
    margin-bottom: 31px;
  }
`;

const Agua = styled.img`
  align-self: center;
  height: 105px;
  margin-bottom: 30px;
  @media (max-width: 425px) {
    height: 80px;
  }
`;

const Titulo = styled.div`
  top: 3em;
  width: 470px;
  align-self: center;
  font-family: Helvetica;
  font-size: 30px;
  color: #6e6e6e;
  text-align: center;
  line-height: 45px;
  @media (max-width: 425px) {
    font-size: 22px;
    width: 230px;
    line-height: 32px;
  }
`;

const CorpoBotao = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: center;
  @media (max-width: 425px) {
    flex-direction: column;
    align-items: center;
  }
`;

const Botao = styled.div`
  height: 215px;
  width: 190px;
  color: ${props => (props.selecionado ? "#ffffff" : "#6e6e6e")};
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  box-shadow: ${props =>
    props.selecionado
      ? "0px 22px 24px 0px #7BAF9E"
      : "0px 22px 24px 0px lightgrey"};
  border-radius: 3px;
  display: flex;
  flex-direction: column;
  text-align: center;
  justify-content:center;
  line-height: 70px;
  align-items: center;
  background-color: ${props => (props.selecionado ? "#7BAF9E" : "#ffffff")};
  @media (max-width: 425px) {
    flex-direction: row;
    width: 80%;
    height: 74px;
  }
`;

const ImagemBotao = styled.img`
  height: 68px;
  @media (max-width: 425px) {
    height: 42px;
    margin-right: 20px;
  }
`;

const BotaoAnterior = styled.div`
  border-radius: 36px;
  background: #ffffff;
  border: 1px solid #15573f;
  font-family: Helvetica;
  font-size: 16px;
  color: #15573f;
  text-align: center;
  width: 170px;
  height: 50px;
  line-height: 3em;
  position: relative;
  right: 19.5em;
  background-color: #f6f6f6;
  &:hover {
    color: #ffffff;
    background-color: #15573f;
  }
  @media (max-width: 425px) {
    flex-direction: row;
    width: 146px;
    height: 50px;
    right: 0px;
    &:hover {
      color: #ffffff;
      background-color: #15573f;
    }
  }
`;

const BotaoProximo = styled.div`
  border-radius: 36px;
  background: #ffffff;
  border: 1px solid #15573f;
  font-family: Helvetica;
  font-size: 16px;
  color: #15573f;
  text-align: center;
  width: 170px;
  height: 50px;
  position: relative;
  line-height: 3em;
  left: 19.5em;
  background-color: #f6f6f6;
  &:hover {
    color: #ffffff;
    background-color: #15573f;
  }
  @media (max-width: 425px) {
    flex-direction: row;
    margin-bottom:31px;
    width: 146px;
    height: 50px;
    left: 0px;
    &:hover {
      color: #ffffff;
      background-color: #15573f;
    }
  }
`;

const CorpoBotao2 = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  margin-top: 46px;
  margin-bottom:31px;
  justify-content: center;
  @media (max-width: 425px) {
    flex-direction: column;
    align-items: center;
  }
`;

const ContainerTexto = styled.div`
display:flex;
flex:1;
`;

const Texto = styled.p`
margin:0;
`;

const Regamento = props => {
  const [selecionado, setselecionado] = useState(0);
  const statusselecionado = (index, resposta) =>
    !!(selecionado === index || props.resposta === resposta);
  return (
    <Tudo>
      <ImgLateral>
        <LogoLateral src={logo} alt="logo"/>
        <LogoMobile src={logomobile} alt="logo2" />
        <LinhaVertical/>
      </ImgLateral>
      <Conteudo>
      <Cabecalho>
          <Agua src={agua} alt="regador" />
        <Titulo>How often do you want to water your plant?</Titulo>
      </Cabecalho>
      <CorpoBotao>
        <br></br>
        <Botao
          onClick={() => props.setresposta("rarely")}
          onMouseEnter={() => setselecionado(1)}
          onMouseLeave={() => setselecionado(0)}
          selecionado={statusselecionado(1, "rarely")}
        >
          {!statusselecionado(1, "rarely") && (
            <ImagemBotao src={gota} alt="rarely water" />
          )}
          {statusselecionado(1, "rarely") && (
            <ImagemBotao src={gotaw} alt="rarely water" />
          )}
          <ContainerTexto><Texto>Rarely</Texto></ContainerTexto>
        </Botao>
        <Botao
          onClick={() => props.setresposta("regularly")}
          onMouseEnter={() => setselecionado(2)}
          onMouseLeave={() => setselecionado(0)}
          selecionado={statusselecionado(2, "regularly")}
        >
          {!statusselecionado(2, "regularly") && (
            <ImagemBotao src={duasgotas} alt="regularly water" />
          )}
          {statusselecionado(2, "regularly") && (
            <ImagemBotao src={dgotaw} alt="regularly water" />
          )}
          <ContainerTexto><Texto>Regularly</Texto></ContainerTexto>
        </Botao>
        <Botao
          onClick={() => props.setresposta("daily")}
          onMouseEnter={() => setselecionado(3)}
          onMouseLeave={() => setselecionado(0)}
          selecionado={statusselecionado(3, "daily")}
        >
          {!statusselecionado(3, "daily") && (
            <ImagemBotao src={tresgotas} alt="daily water" />
          )}
          {statusselecionado(3, "daily") && (
            <ImagemBotao src={tgotaw} alt="daily water" />
          )}
          <ContainerTexto><Texto>Daily</Texto></ContainerTexto>
        </Botao>
      </CorpoBotao>
      <CorpoBotao2>
      <BotaoProximo onClick={props.setnext}>next</BotaoProximo>
        <BotaoAnterior onClick={props.setback}>previous</BotaoAnterior>
      </CorpoBotao2>
      </Conteudo>
    </Tudo>
  );
};

export default Regamento;
